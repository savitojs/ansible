---
- name: Install dependancies for Docker
  apt:
    name: apt-transport-https
  tags: discourse

- name: Allow HTTPS outgoing trafic (for apt.dockerproject.org)
  ufw:
    name: "WWW Secure"
    rule: allow
    direction: out
  tags:
    - discourse
    - firewall

- name: Fetch docker's GPG public key
  apt_key:
    url: https://download.docker.com/linux/debian/gpg
  tags: discourse

- name: Add docker repository
  apt_repository:
    filename: docker
    repo: deb https://download.docker.com/linux/debian stretch stable
    update_cache: yes
  tags: discourse

- name: Pin docker-ce package
  copy:
    src: docker.pref
    dest: /etc/apt/preferences.d/
    mode: "0644"
  tags: discourse

- name: Install dependancies for Discourse
  apt:
    name:
    - docker-ce
    - git
  tags: discourse

- name: Install dependancies for Discourse (incoming mail support)
  apt:
    name: dovecot-pop3d
  when: discourse_enable_incoming_mail
  tags: discourse

- name: Add dovecot configuration
  copy:
    src: dovecot/local.conf
    dest: /etc/dovecot/
    mode: 0600
  notify: Reload Dovecot
  when: discourse_enable_incoming_mail
  tags: discourse

- name: Add unix account for discourse mails
  user:
    name: discourse-mail
    home: /var/mail/discourse-mail
    shell: /bin/false
  when: discourse_enable_incoming_mail
  tags: discourse

- name: Add logcheck exclude rules for Dovecot
  copy:
    src: dovecot/dovecot-local
    dest: /etc/logcheck/ignore.d.server/
    mode: 0644
  when: discourse_enable_incoming_mail
  tags:
    - discourse
    - logcheck

- name: Allow HTTP(S) inbound trafic
  ufw:
    name: WWW Full
    rule: allow
    direction: in
  tags:
    - discourse
    - firewall

- name: Allow SMTP inbound trafic
  ufw:
    name: SMTP
    rule: allow
    direction: in
  when: discourse_enable_incoming_mail
  tags:
    - discourse
    - firewall

- name: Allow container to relay emails throught Postfix on host
  ufw:
    name: Postfix
    rule: allow
    direction: in
    interface: docker0
    src: "172.17.0.0/16"
  tags:
    - discourse
    - firewall

- name: Allow container to poll remote POPS server
  ufw:
    rule: allow
    direction: in
    interface: docker0
    src: "172.17.0.0/16"
    proto: tcp
    port: 995
  tags:
    - discourse
    - firewall

- name: Create dedicated LV for Discourse
  lvol:
    lv: discourse
    size: 20g
    vg: vg0
  register: create_lv
  tags: discourse

- name: Format newly created LV
  filesystem:
    dev: /dev/vg0/discourse
    fstype: ext4
  when: create_lv.changed
  tags: discourse

- name: Create mount point directory
  file:
    name: /srv/discourse
    state: directory
    mode: "0755"
  tags: discourse

- name: Add volume to fstab
  mount:
    src: /dev/vg0/discourse
    path: /srv/discourse
    fstype: ext4
    state: mounted
  tags: discourse

- name: Clone discourse repository
  git:
    name: https://github.com/discourse/discourse_docker.git
    dest: /srv/discourse/discourse/
    clone: yes
  tags: discourse

- name: Template app.yml
  template:
    src: app.yml
    dest: /srv/discourse/discourse/containers/app.yml
  notify: Rebuild container
  tags: discourse
