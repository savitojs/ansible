---
- name: Install munin-node and its plugins
  apt:
    name: ['munin-node', 'munin-plugins-core', 'munin-plugins-extra']
  tags: munin-node

- name: Create directory for additional munin plugins
  file:
    name: /usr/local/share/munin/plugins/
    mode: "0755"
    state: directory
    recurse: true
  tags: munin-node

- name: Configure munin-node
  template:
    src: munin-node.conf
    dest: /etc/munin/
  notify: Restart munin-node
  tags: munin-node

- name: Open munin-node port to munin-master
  ufw:
    rule: allow
    direction: in
    proto: tcp
    from: "{{ hostvars[hosted_by].ansible_all_ipv4_addresses |ipaddr('10.0.0.0/24') |first }}"
    port: 4949
  when: hosted_by is defined
  tags:
    - munin-node
    - molecule-converge-notest

- name: Open munin-node port to munin-master
  ufw:
    rule: allow
    direction: in
    proto: tcp
    from: "{{ hostvars[munin_master].ansible_default_ipv4.address }}"
    port: 4949
  when: hosted_by is not defined and munin_master != ansible_hostname
  tags:
    - munin-node
    - molecule-converge-notest

- name: Remove useless munin plugins
  file:
    name: "/etc/munin/plugins/{{ item }}"
    state: absent
  with_items:
    - bonding_err_bond0
    - if_bond0
    - if_dummy0
    - if_err_bond0
    - if_err_dummy0
    - if_err_eth1
    - if_err_ifb0
    - if_err_ifb1
    - if_err_ip6tnl0
    - if_err_teql0
    - if_err_tunl0
    - if_eth1
    - if_ifb0
    - if_ifb1
    - if_ip6tnl0
    - if_teql0
    - if_tunl0
    - exim_mailqueue
    - exim_mailstats
    - uptime
    - ntp_kernel_err
    - ntp_kernel_pll_freq
    - ntp_kernel_pll_off
    - ntp_offset
    - http_loadtime
  notify: Restart munin-node
  tags: munin-node

- name: Add usefull munin plugins
  file:
    name: "/etc/munin/plugins/{{ item }}"
    src: "/usr/share/munin/plugins/{{ item }}"
    state: link
  with_items:
    - meminfo
    - netstat_multi
    - tcp
    - postfix_mailqueue
    - postfix_mailstats
    - postfix_mailvolume
  notify: Restart munin-node
  tags: munin-node

- name: More plugins for harware hosts
  file:
    name: /etc/munin/plugins/sensors_temp
    src: /usr/share/munin/plugins/sensors_
    state: link
  when: ansible_virtualization_role != "guest"
  notify: Restart munin-node
  tags: munin-node

- name: Install logcheck
  apt:
    name: ['logcheck', 'logcheck-database']
  tags: logcheck

- name: Add logcheck configuration
  copy:
    src: '{{ item }}'
    dest: /etc/logcheck/
    mode: 0644
  with_items: ['logcheck.conf', 'logcheck.logfiles']
  tags: logcheck

- name: Add logcheck exclude rules
  copy:
    src: '{{ item }}'
    dest: /etc/logcheck/ignore.d.server/
    mode: 0644
  with_fileglob: logcheck-ignore/*
  tags: logcheck

- block:
    - name: Install dependencies for log2xmpp
      apt:
        name:
          - python3-daemon
          - python3-sleekxmpp
          - python3-regex
      tags: log2xmpp

    - name: Copy log2xmpp script
      copy:
        src: log2xmpp.py
        dest: /usr/local/bin/
        mode: "0755"
      tags: log2xmpp

    - name: Create system user for log2xmpp
      user:
        name: log2xmpp
        home: /var/lib/log2xmpp
        create_home: true
        shell: /bin/false
        system: true
        groups: logcheck
      tags: log2xmpp

    - name: Copy systemd unit
      template:
        src: log2xmpp.service
        dest: /etc/systemd/system/
        mode: "0600"
      notify: Restart log2xmpp
      tags: log2xmpp

    - name: Enable log2xmpp on boot
      systemd:
        name: log2xmpp
        daemon_reload: true
        enabled: true
      tags: log2xmpp

    - name: Allow outgoing XMPP trafic for log2xmpp
      ufw:
        rule: allow
        dest: "{{ log2xmpp_xmpp_host }}"
        proto: tcp
        port: 5222
        direction: out
      when: hosted_by is not defined
      tags:
        - log2xmpp
        - firewall
        - molecule-converge-notest

  when: hosted_by is not defined
